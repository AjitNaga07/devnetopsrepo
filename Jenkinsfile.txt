pipeline {
    agent any
    stages {
        stage('Clean and prep workspace') {
            steps {
                git 'https://gitlab.com/jaya-narayana/devnetopscicdpipeline.git'
            }
        }
        stage('Playbook Sanity test') {
            steps {
                sh 'ansible-lint -x 201,403 playbook_httpd.yml'
            }
        }
        stage('Ansible Run playbook') {
            steps {
                sh 'ansible-playbook playbook_httpd.yml '
            }
        }
    }
}
